package com.model.state;
import com.service.ScanContext;

public interface EtatScan {
    void demarrerScan(ScanContext context);
    void terminerScan(ScanContext context);
    void erreurScan(ScanContext context);
}

